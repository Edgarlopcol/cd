

""" 
    Prueba Edgar

1- Crear una Clase Producto con los siguientes atributos:
    -Código
    -Nombre
    -precio
Crea su constructor, getters y setters y una función llamada calcular total,
donde le pasaremos unas unidades y nos tiene calcular el precio final. """

class Prodcuto:
    def __init__(self, pCodigo, pNombre, pPrecio):
        self.__codigo = pCodigo
        self.__nombre = pNombre
        self.__precio = pPrecio

    def SetCodigo(self, pCodigo):
        self.__codigo = pCodigo
    def SetNombre(self, pNombre):
        self.__nombre = pNombre
    def SetPrecio(self, pPrecio):
        self.__precio = pPrecio

    def GetCodigo(self):
        return self.__codigo
    def GetNombre(self):
        return self.__nombre
    def GetPrecio(self):
        return self.__precio

    def CalcularTotal(self,pCantidad):
        self.__cantidad = pCantidad
        self.__total = (self.__cantidad * self.__precio)
        return str(self.__total)

    def __str__(self):
        return "PRODUCTO: " + str(self.GetCodigo()) + " | Nombre: " + self.GetNombre()+ " | Precio: " + str(self.GetPrecio())

class pedidos:
    def __init__(self, pListaProductos):
        self.__listaProductos = pListaProductos
    
    def GetListaProductos(self):
        return self.__listaProductos

p1 = Prodcuto(1,"Producto A",8)
p2 = Prodcuto(2,"Producto B",20)
p3 = Prodcuto(3,"Producto C",16)

listaProductos =[p1,p2,p3]
mostrar = pedidos(listaProductos)

for contador in mostrar.GetListaProductos():
    print(contador)

cantidad = 5

print(p1.CalcularTotal(cantidad))


